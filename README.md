# ai.txt

Using an **ai.txt** file in your website's root folder is beneficial because it guides AI systems on how to interact with your site, similar to robots.txt for web crawlers. This helps protect privacy, manage server load, ensure ethical AI use, and demonstrate transparency and accountability, fostering trust with users and AI developers. See [Spawning AI](https://site.spawning.ai/spawning-ai-txt) for more information.

## About

Please find a thorough description of the purpose of the ai.txt file in this [blog post](https://spawning.substack.com/p/aitxt-a-new-way-for-websites-to-set) by Spawning. Note that "Spawning views ai.txt not as a unilateral standard, but rather as a simple and useful way to declare permissions for text and data mining."

## How to use?

1. **Installation:** Start by installing the module via Composer. Then enable the module.
2. **Configure Settings:** Go to _Administration > Configuration > Search and metadata > ai.txt_ and configure the ai.txt file content. You may choose to generate the file with predefined extensions or set the content manually.
3. **Path:** Navigate to the file via the /ai.txt path.<

## Troubleshooting

If the /ai.txt path is not returning your configured file, check:

  - Do you have an ai.txt file on the webroot?
  - Do you have clean URLs disabled?
  - Do you have the fast 404 feature enabled?

## Frequently Asked Questions

**Q:** Can this module work if I have clean URLs disabled?

**A:** Yes it can! In the .htaccess file of your Drupal's root directory, add the following line to the mod_rewrite section, immediately after the line that says "RewriteEngine on":

```
RewriteRule ^(ai.txt)$ index.php?q=$1
```

**Q:** Does this module work together with Drupal Core "Fast 404 pages" feature?

**A:** Yes, but you need to add ai.txt to the 'exclude_paths' of your settings.php.

* Default Drupal Fast404 configuration (in settings.php):
```
$config['system.performance']['fast_404']['exclude_paths'] =
   '/\/(?:styles)|(?:system\/files)\//';
```

* New Drupal Fast404 configuration (in settings.php) to allow ai.txt module:
```
$config['system.performance']['fast_404']['exclude_paths'] =
  '/\/(?:styles)|(?:system\/files)\/|(?:ai.txt)/';
```

**Q:** How can I install the module with custom default ai.txt?

**A:** The module _upon install only_ allows adding a default.ai.txt to the defaults folder.

1. Remove the ai.txt from webroot.
2. Save your custom ai.txt to "/sites/default/default.ai.txt"
3. Run the module installation.

## Developer Notes

You can automatically add paths to the ai.txt file by implementing *hook_aitxt()*. See aitxt.api.php for more documentation.

## Similar Modules and Special Thanks

This module is a fork of [RobotsTxt](https://www.drupal.org/project/robotstxt). Please send your appreciations to the maintainers there.

## Sponsored by Factorial GmbH

This project is sponsored by [Factorial GmbH](https://www.factorial.io). Contact us if you are looking for interesting open-source employment.

## Maintainer

- Simon Bäse
