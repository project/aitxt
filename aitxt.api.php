<?php

/**
 * @file
 * Hooks provided by the ai.txt module.
 */

/**
 * Add additional lines to the site's ai.txt file.
 *
 * @return array
 *   An array of strings to add to the ai.txt.
 */
function hook_aitxt(): array {
  return [
    'Disallow: *.foo',
    'Disallow: *.bar',
  ];
}
