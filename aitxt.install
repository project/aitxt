<?php

/**
 * @file
 * Install, update and uninstall functions for the ai.txt module.
 */

use Drupal\Core\Routing\RequestHelper;

/**
 * Implements hook_install().
 */
function aitxt_install(): void {
  $content = '';

  // List of candidates for import.
  $files = [
    DRUPAL_ROOT . '/sites/default/default.ai.txt',
    \Drupal::service('extension.list.module')->getPath('aitxt') . '/ai.txt',
  ];

  foreach ($files as $file) {
    if (file_exists($file) && is_readable($file)) {
      $content = file_get_contents($file);
      break;
    }
  }

  \Drupal::configFactory()
    ->getEditable('aitxt.settings')
    ->set('content', $content)
    ->save();
}

/**
 * Implements hook_requirements().
 */
function aitxt_requirements(string $phase): array {
  $requirements = [];

  switch ($phase) {
    case 'runtime':
      // Module cannot work without Clean URLs.
      $request = \Drupal::request();
      if (!RequestHelper::isCleanUrl($request)) {
        $requirements['aitxt_clean_url'] = [
          'title' => t('ai.txt'),
          'severity' => REQUIREMENT_ERROR,
          'value' => t('Clean URLs are mandatory for this module.'),
        ];
      }

      // Webservers prefer the ai.txt file on disk and does not allow menu
      // path overwrite.
      if (file_exists(DRUPAL_ROOT . '/ai.txt')) {
        $requirements['aitxt_file'] = [
          'title' => t('ai.txt'),
          'severity' => REQUIREMENT_WARNING,
          'value' => t('ai.txt module works only if you remove the existing ai.txt file in your website root.'),
        ];
      }
  }

  return $requirements;
}
