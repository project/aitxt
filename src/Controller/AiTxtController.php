<?php

namespace Drupal\aitxt\Controller;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Render\HtmlResponse;
use Drupal\Core\Render\RenderContext;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Provides output ai.txt output.
 */
class AiTxtController implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * Constructs a new AiTxtController object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler service.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   */
  public function __construct(
    protected ConfigFactoryInterface $configFactory,
    protected ModuleHandlerInterface $moduleHandler,
    protected RendererInterface $renderer,
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    return new static(
      $container->get('config.factory'),
      $container->get('module_handler'),
      $container->get('renderer')
    );
  }

  /**
   * Serves the configured ai.txt file.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The ai.txt file as a response object with 'text/plain' content type.
   */
  public function content(): Response {
    $context = new RenderContext();

    $response = $this->renderer->executeInRenderContext($context, function () {
      $content = [];
      $content[] = $this->configFactory->get('aitxt.settings')->get('content');

      // Hook other modules for adding additional lines.
      if ($additions = $this->moduleHandler->invokeAll('aitxt')) {
        $content = array_merge($content, $additions);
      }

      // Trim any extra whitespace and filter out empty strings.
      $content = array_map('trim', $content);
      $content = array_filter($content);
      $content = implode("\n", $content);

      // Treat the content as an render array to populate the $context object.
      $build = ['#plain_text' => $content];
      $this->renderer->render($build);

      return new HtmlResponse($build, Response::HTTP_OK, ['content-type' => 'text/plain']);
    });

    // If there is metadata left on the context, apply it on the response.
    if (!$context->isEmpty()) {
      $metadata = $context->pop();
      $metadata->addCacheTags(['aitxt']);
      $metadata->addCacheContexts(['url.site']);
      $response->addCacheableDependency($metadata);
      $response->addAttachments($metadata->getAttachments());
    }

    return $response;
  }

}
