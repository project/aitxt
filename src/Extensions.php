<?php

namespace Drupal\aitxt;

/**
 * Provides file extensions for ai.txt options.
 *
 * Note that the default ai.txt file should be updated, if these file extensions
 * are changed.
 */
class Extensions {

  /**
   * The audio file extensions.
   *
   * @var array
   */
  const AUDIO = [
    '*.aac',
    '*.aiff',
    '*.amr',
    '*.flac',
    '*.m4a',
    '*.mp3',
    '*.oga',
    '*.opus',
    '*.wav',
    '*.wma',
  ];

  /**
   * The code file extensions.
   *
   * @var array
   */
  const CODE = [
    '*.py',
    '*.js',
    '*.java',
    '*.c',
    '*.cpp',
    '*.cs',
    '*.h',
    '*.css',
    '*.php',
    '*.swift',
    '*.go',
    '*.rb',
    '*.pl',
    '*.sh',
    '*.sql',
  ];

  /**
   * The images file extensions.
   *
   * @var array
   */
  const IMAGES = [
    '*.bmp',
    '*.gif',
    '*.ico',
    '*.jpeg',
    '*.jpg',
    '*.png',
    '*.svg',
    '*.tif',
    '*.tiff',
    '*.webp',
  ];

  /**
   * The text file extensions.
   *
   * @var array
   */
  const TEXT = [
    '*.txt',
    '*.pdf',
    '*.doc',
    '*.docx',
    '*.odt',
    '*.rtf',
    '*.tex',
    '*.wks',
    '*.wpd',
    '*.wps',
    '*.html',
  ];

  /**
   * The video file extensions.
   *
   * @var array
   */
  const VIDEO = [
    '*.mp4',
    '*.webm',
    '*.ogg',
    '*.avi',
    '*.mov',
    '*.wmv',
    '*.flv',
    '*.mkv',
  ];

}
