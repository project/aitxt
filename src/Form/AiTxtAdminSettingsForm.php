<?php

namespace Drupal\aitxt\Form;

use Drupal\aitxt\Extensions;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Configure ai.txt settings for this site.
 */
class AiTxtAdminSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'aitxt_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['aitxt.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config('aitxt.settings');

    $form['description'] = [
      '#type' => 'markup',
      '#markup' => '<p>' . $this->t('Feel free to use the toggles to choose whether you want your content to help train AI models. If you select <em>allow</em> for any content type, data miners will know they\'re welcome to use that type of media from your website. See <a href="https://site.spawning.ai/spawning-ai-txt" target="_blank">https://site.spawning.ai/spawning-ai-txt</a> for more information concerning the <a href=":aitxt" target="_blank">ai.txt</a> file.', [':aitxt' => Url::fromUri('base://ai.txt')->toString()]) . '</p>',
    ];

    $form['manual'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Manual content'),
      '#default_value' => $config->get('manual'),
    ];

    $form['allow'] = [
      '#type' => 'details',
      '#title' => $this->t('Generate with predefined extensions'),
      '#open' => TRUE,
      '#states' => [
        'open' => [
          ':input[name="manual"]' => ['checked' => FALSE],
        ],
      ],
    ];

    $form['allow']['text'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow text'),
      '#default_value' => $config->get('allow_text'),
      '#states' => [
        'disabled' => [
          ':input[name="manual"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['allow']['images'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow images'),
      '#default_value' => $config->get('allow_images'),
      '#states' => [
        'disabled' => [
          ':input[name="manual"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['allow']['audio'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow audio'),
      '#default_value' => $config->get('allow_audio'),
      '#states' => [
        'disabled' => [
          ':input[name="manual"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['allow']['video'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow video'),
      '#default_value' => $config->get('allow_video'),
      '#states' => [
        'disabled' => [
          ':input[name="manual"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['allow']['code'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow code'),
      '#default_value' => $config->get('allow_code'),
      '#states' => [
        'disabled' => [
          ':input[name="manual"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['content'] = [
      '#type' => 'textarea',
      '#title' => $this->t('ai.txt'),
      '#description' => $this->t('Current contents of the ai.txt file. Save the form to see changes.'),
      '#default_value' => $config->get('content'),
      '#cols' => 60,
      '#rows' => 20,
      '#states' => [
        'disabled' => [
          ':input[name="manual"]' => ['checked' => FALSE],
        ],
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $config = $this->config('aitxt.settings');
    $manual = (bool) $form_state->getValue('manual');

    if ($manual) {
      $content = [];
      $content[] = $form_state->getValue('content');
    }
    else {
      $allow_text = (bool) $form_state->getValue('text');
      $allow_images = (bool) $form_state->getValue('images');
      $allow_audio = (bool) $form_state->getValue('audio');
      $allow_video = (bool) $form_state->getValue('video');
      $allow_code = (bool) $form_state->getValue('code');
      $content = ['User-Agent: *'];

      $allowed = array_merge(
        $allow_text ? Extensions::TEXT : [],
        $allow_images ? Extensions::IMAGES : [],
        $allow_audio ? Extensions::AUDIO : [],
        $allow_video ? Extensions::VIDEO : [],
        $allow_code ? Extensions::CODE : [],
      );

      $disallowed = array_merge(
        !$allow_text ? Extensions::TEXT : [],
        !$allow_images ? Extensions::IMAGES : [],
        !$allow_audio ? Extensions::AUDIO : [],
        !$allow_video ? Extensions::VIDEO : [],
        !$allow_code ? Extensions::CODE : [],
      );

      foreach ($allowed as $extension) {
        $content[] = 'Allow: ' . $extension;
      }
      foreach ($disallowed as $extension) {
        $content[] = 'Disallow: ' . $extension;
      }
      $content[] = $form_state->getValue('allow_text') ? 'Allow: /' : 'Disallow: /';
    }

    // Trim any extra whitespace and filter out empty strings.
    $content = array_map('trim', $content);
    $content = array_filter($content);
    $content = implode("\n", $content);

    // Normalizing config data before saving.
    // @todo Revisit this when these land in core:
    // https://www.drupal.org/project/drupal/issues/3202631
    // https://www.drupal.org/project/drupal/issues/3202796
    $content = preg_replace('/\r\n?/', "\n", $content);

    $config->set('content', $content)
      ->set('manual', $manual)
      ->set('allow_text', $allow_text ?? FALSE)
      ->set('allow_images', $allow_images ?? FALSE)
      ->set('allow_audio', $allow_audio ?? FALSE)
      ->set('allow_video', $allow_video ?? FALSE)
      ->set('allow_code', $allow_code ?? FALSE)
      ->save();

    Cache::invalidateTags(['aitxt']);
    parent::submitForm($form, $form_state);
  }

}
