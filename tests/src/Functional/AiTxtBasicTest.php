<?php

namespace Drupal\Tests\aitxt\Functional;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Tests\BrowserTestBase;
use Drupal\user\Entity\User;

/**
 * Tests basic functionality of configured ai.txt files.
 *
 * @group aitxt
 */
class AiTxtBasicTest extends BrowserTestBase {

  use StringTranslationTrait;

  /**
   * Provides the default theme.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * Modules to enable.
   *
   * @var string[]
   */
  protected static $modules = ['aitxt', 'node', 'aitxt_test'];

  /**
   * User with proper permissions for module configuration.
   *
   * @var \Drupal\user\Entity\User
   */
  protected User $adminUser;

  /**
   * User with content access.
   *
   * @var \Drupal\user\Entity\User
   */
  protected User $normalUser;

  /**
   * Checks that an administrator can view the configuration page.
   */
  public function testAiTxtAdminAccess(): void {
    // Create user.
    /** @var \Drupal\user\Entity\User $admin_user */
    $admin_user = $this->drupalCreateUser(['administer aitxt']);
    $this->adminUser = $admin_user;
    $this->drupalLogin($this->adminUser);
    $this->drupalGet('admin/config/search/aitxt');

    // The textarea for configuring ai.txt is shown.
    $this->assertSession()->fieldExists('content');
  }

  /**
   * Checks that a non-administrative user cannot use the configuration page.
   */
  public function testAiTxtUserNoAccess(): void {
    // Create user.
    /** @var \Drupal\user\Entity\User $normal_user */
    $normal_user = $this->drupalCreateUser(['access content']);
    $this->normalUser = $normal_user;
    $this->drupalLogin($this->normalUser);
    $this->drupalGet('admin/config/search/aitxt');

    $this->assertSession()->statusCodeEquals(403);

    // The textarea for configuring ai.txt is not shown for users without
    // appropriate permissions.
    $this->assertSession()->fieldNotExists('edit-content');
  }

  /**
   * Test that the ai.txt path delivers content with an appropriate header.
   */
  public function testAiTxtPath(): void {
    $this->drupalGet('ai-test.txt');

    // No local ai.txt file was detected, and an anonymous user is delivered
    // content at the /ai.txt path.
    $this->assertSession()->statusCodeEquals(200);

    // The ai.txt file was served with header
    // Content-Type: "text/plain; charset=UTF-8".
    $this->assertSession()->responseHeaderEquals('Content-Type', 'text/plain; charset=UTF-8');
  }

  /**
   * Test that the ai.txt path delivers content the appropriate cache tags.
   */
  public function testAiTxtCacheTags(): void {
    $this->drupalGet('ai-test.txt');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->responseHeaderContains('X-Drupal-Cache-Tags', 'aitxt');
  }

  /**
   * Checks that a configured ai.txt file is delivered as configured.
   */
  public function testAiTxtConfigureAiTxt(): void {
    // Create an admin user, log in and access settings form.
    /** @var \Drupal\user\Entity\User $admin_user */
    $admin_user = $this->drupalCreateUser(['administer aitxt']);
    $this->adminUser = $admin_user;
    $this->drupalLogin($this->adminUser);
    $this->drupalGet('admin/config/search/aitxt');

    $test_string = "# SimpleTest {$this->randomMachineName()}";
    $this->submitForm(['content' => $test_string, 'manual' => TRUE], $this->t('Save configuration'));

    $this->drupalLogout();
    $this->drupalGet('ai-test.txt');

    // No local ai.txt file was detected, and an anonymous user is delivered
    // content at the /ai-test.txt path.
    $this->assertSession()->statusCodeEquals(200);

    // The ai.txt file was served with header
    // Content-Type: "text/plain; charset=UTF-8".
    $this->assertSession()->responseHeaderEquals('Content-Type', 'text/plain; charset=UTF-8');
    $content = $this->getSession()->getPage()->getContent();
    // @phpstan-ignore-next-line
    $this->assertEquals($content, $test_string, sprintf('Test string [%s] is displayed in the configured ai.txt file [%s].', $test_string, $content));
  }

}
